#!/usr/bin/python
import logging
import argparse
import SocketServer
import threading

from sense_hat import SenseHat

logging.basicConfig(level=logging.DEBUG,
                    format='%(name)s: %(message)s',
                    )

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

class HatTCPHandler(SocketServer.BaseRequestHandler):
    def handle(self):
        logging.debug('Connection from %s', self.client_address)
        self.run = True
        data = ""
	while self.run:
            data += self.request.recv(1024)
            if not data:
                break
            while '\n' in data:
                ind = data.index('\n') + 1
                command = data[:ind]
                data = data[ind:]
                self.execute(command)

    def execute(self, command):
        v = command.split()
        code = v[0]
        v = v[1:]
        if code == 'q':
            self.run = False
        elif code == 'p':
            x, y, r, g, b = map(int, v)
            hat.set_pixel(x, y, r, g, b)
        elif code == 'c':
            hat.set_pixels([[0, 0, 0]] * 64)
        elif code == 'm':
            assert len(v) == 64 * 3
            v = map(int, v)
            hat.set_pixels([[v[i], v[i + 1], v[i + 2]] for i in xrange(0, 64 * 3, 3)])

    def finish(self):
        logging.debug('Closed connection from %s', self.client_address)

if __name__ == '__main__':
    hat = SenseHat()

    parser = argparse.ArgumentParser(description='Sense Hat server')
    parser.add_argument('--host', '-H', default='localhost')
    parser.add_argument('--port', '-p', type=int, default=9993)
    args = parser.parse_args()

    print 'Listening: %s:%d' % (args.host, args.port)
    server = ThreadedTCPServer((args.host, args.port), HatTCPHandler)
    server.serve_forever()

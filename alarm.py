#!/usr/bin/env python
from time import sleep
import traceback

from sense_hat import SenseHat


formula = (0.8, 0.8, 1)
def palletter(start, end, i, nstep):
    prop = float(i) / (nstep - 1)
    dist = [end[i] - start[i] for i in xrange(3)]
    return [int(start[i] + prop * dist[i]) for i in xrange(3)]

def gradient(hat, start, end, f=lambda: None, N=100):
    for i in xrange(N):
        col=palletter(start, end, i, N)
        hat.clear(col)
        f()

def blink(hat, col, period, times):
    for _ in xrange(times):
        hat.clear()
        sleep(float(period)/2)
        hat.clear(col)
        sleep(float(period)/2)


def wakeup(hat):
    gradient(hat, (0, 0, 0), (200, 200, 255))
    gradient(hat, (200, 200, 255), (255, 255, 0))
    blink(hat, (255, 255, 0), 5, 12)
    blink(hat, (200, 200, 255), 5, 12)
    blink(hat, (255, 255, 0), 1, 150)
    blink(hat, (200, 200, 255), 1, 150)


if __name__ == '__main__':
    hat = SenseHat()
    hat.clear()
    try:
        wakeup(hat)
    except:
        traceback.print_exc()
    hat.clear()
	
